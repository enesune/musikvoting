package de.TheSolvers.Enesu;

import java.awt.EventQueue;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.util.Locale;

import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JToolBar;
import java.awt.Cursor;

public class Gui extends JFrame{
	GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	int width = gd.getDisplayMode().getWidth();
	int height = gd.getDisplayMode().getHeight();

	JInternalFrame internalFrame = new JInternalFrame("New JInternalFrame");
	
	public Gui() {
		setResizable(false);
		getContentPane().setEnabled(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH); 
		setUndecorated(true);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Gui.class.getResource("/de/TheSolvers/Enesu/SoundOfAxeAndFire.png")));

		
		frame();
		frame2();
		
		
		/**JInternalFrame internalFrame_1 = new JInternalFrame("LOGIN");
		internalFrame_1.setBounds(214, 129, 517, 437);
		internalFrame_1.setClosable(true);
		internalFrame.getContentPane().add(internalFrame_1);
		internalFrame_1.getContentPane().setLayout(new BoxLayout(internalFrame_1.getContentPane(), BoxLayout.X_AXIS));
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, width, 27);
		internalFrame.getContentPane().add(toolBar);
		
		JButton btnLogin = new JButton("Login");
		toolBar.add(btnLogin);
		
		JButton btnMusicEintragen = new JButton("Music eintragen");
		toolBar.add(btnMusicEintragen);
		
		JButton btnVoting = new JButton("Voting");
		toolBar.add(btnVoting);
		internalFrame_1.setVisible(true);*/
		
		
		 
			
			
		
	}
	
	public void frame() {
		internalFrame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		internalFrame.setFrameIcon(new ImageIcon(Gui.class.getResource("/de/TheSolvers/Enesu/SoundOfAxeAndFire.png")));
		internalFrame.setLocale(Locale.US);
		internalFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		internalFrame.setToolTipText("Hi");
		internalFrame.setClosable(true);
		internalFrame.setVisible(true);
		internalFrame.setTitle("Sound of Axe and Fire");
		//internalFrame.setUndecorated(false);
		getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
		getContentPane().add(internalFrame);
		internalFrame.getContentPane().setLayout(null);		
	}
	
	
	public void frame2() {
		JInternalFrame internalFrame_1 = new JInternalFrame("");
		internalFrame_1.setIconifiable(true);
		internalFrame_1.setFrameIcon(new ImageIcon(Gui.class.getResource("/de/TheSolvers/Enesu/Login.png")));
		internalFrame_1.setBounds(214, 129, 517, 437);
		internalFrame_1.setClosable(true);
		internalFrame.getContentPane().add(internalFrame_1);
		internalFrame_1.getContentPane().setLayout(new BoxLayout(internalFrame_1.getContentPane(), BoxLayout.X_AXIS));
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBounds(0, 0, width, 27);
		
		internalFrame.getContentPane().add(toolBar);
		
		JButton btnLogin = new JButton("Login");
		toolBar.add(btnLogin);
		
		JButton btnMusicEintragen = new JButton("Music eintragen");
		toolBar.add(btnMusicEintragen);
		
		JButton btnVoting = new JButton("Voting");
		toolBar.add(btnVoting);
		internalFrame_1.setVisible(true);
			
		}
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Gui windows = new Gui();
					windows.setResizable(true); //actually true!
					windows.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
}
