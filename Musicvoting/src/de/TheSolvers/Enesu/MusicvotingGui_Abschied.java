package de.TheSolvers.Enesu;

import javax.swing.JFrame;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.GridBagConstraints;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JList;
import javax.swing.JSpinner;
import javax.swing.JButton;

public class MusicvotingGui_Abschied extends JFrame {
	private final static boolean true1 = false;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusicvotingGui_Abschied windows = new MusicvotingGui_Abschied();
					windows.setResizable(true1); // actually true!
					windows.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MusicvotingGui_Abschied() {

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Sound of Axe and Fire");
		setSize(400, 208);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(MusicvotingGui_Login.class.getResource("/de/TheSolvers/Enesu/SoundOfAxeAndFire.png")));
		getContentPane().setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 198, 200, 0 };
		gridBagLayout.rowHeights = new int[] { 48, 31, 0, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JLabel lblHerzlichWillkommenBeim = new JLabel(
				"<html><body><center>Vielen Dank fuer die<br>Nutzung unserer Software!</center></body></html>");
		lblHerzlichWillkommenBeim.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 20));
		GridBagConstraints gbc_lblHerzlichWillkommenBeim = new GridBagConstraints();
		gbc_lblHerzlichWillkommenBeim.gridwidth = 2;
		gbc_lblHerzlichWillkommenBeim.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblHerzlichWillkommenBeim.anchor = GridBagConstraints.SOUTH;
		gbc_lblHerzlichWillkommenBeim.insets = new Insets(10, 80, 5, 0);
		gbc_lblHerzlichWillkommenBeim.gridx = 0;
		gbc_lblHerzlichWillkommenBeim.gridy = 0;
		getContentPane().add(lblHerzlichWillkommenBeim, gbc_lblHerzlichWillkommenBeim);

		JButton btnNewButton = new JButton("wiederholen");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 0;
		gbc_btnNewButton.gridy = 3;
		getContentPane().add(btnNewButton, gbc_btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MusicvotingGui_Login.main(null);
				setVisible(false);
			}

		});

		JButton btnNewButton_1 = new JButton("beenden");
		btnNewButton_1.setToolTipText("YOHoo Yohoo");
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.gridx = 1;
		gbc_btnNewButton_1.gridy = 3;
		getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}

		});
	}

}