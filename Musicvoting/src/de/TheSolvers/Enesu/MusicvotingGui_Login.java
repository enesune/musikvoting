package de.TheSolvers.Enesu;

import de.TheSolvers.InguantaElias.LogicSchicht;
import de.TheSolvers.InguantaElias.Person;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Dimension;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MusicvotingGui_Login extends JFrame implements ActionListener {

	private static final long serialVersionUID = -4288225287544322629L;
	private JPanel contentPane;
	private static GridBagConstraints gbc_login = new GridBagConstraints();
	private JLabel login;
	private final static boolean true1 = false;
	private JTextField textField;
	private JButton start;
	private JButton abbort;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusicvotingGui_Login windows = new MusicvotingGui_Login();
					windows.setResizable(true1); // actually true!
					windows.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MusicvotingGui_Login() {

		LogicSchicht logic = new LogicSchicht();
		Person person = new Person();
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		setTitle("Sound of Axe and Fire");
		setBackground(new Color(240, 240, 240));
		setAlwaysOnTop(true);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(MusicvotingGui_Login.class.getResource("/de/TheSolvers/Enesu/SoundOfAxeAndFire.png")));
		contentPane = new JPanel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 230);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0 };
		gridBagLayout.rowHeights = new int[] { 0, 71, 10, 0 };
		gridBagLayout.columnWidths = new int[] { -27, 100, 100 };
		gridBagLayout.columnWeights = new double[] { 78.0, 78.0, 78.0 };
		getContentPane().setLayout(gridBagLayout);

		JLabel lblNewLabel_1 = new JLabel();
		lblNewLabel_1.setIcon(new ImageIcon(
				MusicvotingGui_Login.class.getResource("/de/TheSolvers/Enesu/SoundOfAxeAndFireSmall2.gif")));
		lblNewLabel_1.setToolTipText("The Solvers");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(5, 0, 5, 5);
		gbc_lblNewLabel_1.gridx = 0;
		gbc_lblNewLabel_1.gridy = 1;
		getContentPane().add(lblNewLabel_1, gbc_lblNewLabel_1);

		login = new JLabel("Login");
		login.setLabelFor(getContentPane());
		login.setFont(new Font("Bradley Hand ITC", Font.BOLD, 50));
		gbc_login.insets = new Insets(20, 0, 5, 0);
		gbc_login.anchor = GridBagConstraints.WEST;
		gbc_login.gridwidth = 2;
		gbc_login.gridx = 1;
		gbc_login.gridy = 1;
		getContentPane().add(login, gbc_login);

		JLabel lblNewLabel = new JLabel("Name:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 20));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.EAST;
		gbc_lblNewLabel.insets = new Insets(0, 5, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 2;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		textField = new JTextField();
		GridBagConstraints gbc_textField = new GridBagConstraints();
		// textField.setText("name");
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets(0, 0, 0, 50);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 1;
		gbc_textField.gridy = 2;
		getContentPane().add(textField, gbc_textField);
		textField.setColumns(10);

		start = new JButton("Start");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(10, 20, 5, 50);
		gbc_btnNewButton.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton.gridx = 2;
		gbc_btnNewButton.gridy = 3;
		getContentPane().add(start, gbc_btnNewButton);
		start.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MusicvotingGui_MusicEintragen.main(null);
				setVisible(false);
				person.setName(new String(textField.getText()));
				logic.einloggen(new String(textField.getText()));

			}

		});

		abbort = new JButton("(Ab)brechen");

		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.insets = new Insets(10, 0, 5, 5);
		gbc_btnNewButton_1.gridx = 1;
		gbc_btnNewButton_1.gridy = 3;

		getContentPane().add(abbort, gbc_btnNewButton_1);

		abbort.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub

	}

}
