package de.TheSolvers.Enesu;

import de.TheSolvers.InguantaElias.*;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class MusicvotingGui_MusicEintragen extends JFrame implements ActionListener {

	private static final long serialVersionUID = -1523120877044178765L;
	static GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

	private JPanel contentPane;
	private static GridBagConstraints gbc_login = new GridBagConstraints();
	private final static boolean true1 = false;
	private JLabel login;
	private JTextField textField;
	private JButton btnNewButton;
	private JLabel lblNewLabel_1;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;

	// private static GridBagConstraints constraint = new GridBagConstraints();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					// ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new
					// File("/de/TheSolvers/Enesu/UDDigiKyokashoNP-R.ttf")));
					MusicvotingGui_MusicEintragen windows = new MusicvotingGui_MusicEintragen();
					windows.setResizable(true1); // actually true!
					windows.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/*
	 * try { GraphicsEnvironment ge =
	 * GraphicsEnvironment.getLocalGraphicsEnvironment();
	 * ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File("A.ttf"))); }
	 * catch (IOException|FontFormatException e) { //Handle exception }
	 **/
	public MusicvotingGui_MusicEintragen() {

		// Person person= new Person();
		setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		LogicSchicht logic = new LogicSchicht();

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
		getContentPane().setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		setTitle("Sound of Axe and Fire");
		setBackground(new Color(240, 240, 240));
		setAlwaysOnTop(true);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(MusicvotingGui_Login.class.getResource("/de/TheSolvers/Enesu/SoundOfAxeAndFire.png")));

		contentPane = new JPanel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(400, 278);
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		gridBagLayout.rowHeights = new int[] { 0, 31, 40, 40, 40, 0 };
		gridBagLayout.columnWidths = new int[] { 150, 1, 131, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0 };
		getContentPane().setLayout(gridBagLayout);

		JLabel lblHerzlichWillkommenname = new JLabel("Herzlich Willkommen, " + getName() + ".\r\n");
		lblHerzlichWillkommenname.setToolTipText("Bitte geben Sie den Song mit den folgendem Kriterien an.");
		lblHerzlichWillkommenname.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 20));
		GridBagConstraints gbc_lblHerzlichWillkommenname = new GridBagConstraints();
		gbc_lblHerzlichWillkommenname.anchor = GridBagConstraints.ABOVE_BASELINE;
		gbc_lblHerzlichWillkommenname.gridwidth = 4;
		gbc_lblHerzlichWillkommenname.insets = new Insets(0, 15, 5, 5);
		gbc_lblHerzlichWillkommenname.gridx = 0;
		gbc_lblHerzlichWillkommenname.gridy = 0;
		getContentPane().add(lblHerzlichWillkommenname, gbc_lblHerzlichWillkommenname);

		JLabel lblBitteGebenSie = new JLabel("Bitte geben Sie den Song mit den folgendem Kriterien an.");
		lblBitteGebenSie.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 15));
		GridBagConstraints gbc_lblBitteGebenSie = new GridBagConstraints();
		gbc_lblBitteGebenSie.gridwidth = 4;
		gbc_lblBitteGebenSie.insets = new Insets(0, 0, 5, 0);
		gbc_lblBitteGebenSie.gridx = 0;
		gbc_lblBitteGebenSie.gridy = 1;
		getContentPane().add(lblBitteGebenSie, gbc_lblBitteGebenSie);

		JLabel lblNewLabel_2 = new JLabel("Bandname");
		lblNewLabel_2.setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagConstraints gbc_lblNewLabel_2 = new GridBagConstraints();
		gbc_lblNewLabel_2.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_2.insets = new Insets(0, 50, 5, 5);
		gbc_lblNewLabel_2.gridx = 0;
		gbc_lblNewLabel_2.gridy = 2;
		getContentPane().add(lblNewLabel_2, gbc_lblNewLabel_2);

		JLabel lblNewLabel_4 = new JLabel(":");
		lblNewLabel_4.setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagConstraints gbc_lblNewLabel_4 = new GridBagConstraints();
		gbc_lblNewLabel_4.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel_4.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_4.gridx = 1;
		gbc_lblNewLabel_4.gridy = 2;
		getContentPane().add(lblNewLabel_4, gbc_lblNewLabel_4);

		textField_1 = new JTextField();
		GridBagConstraints gbc_textField_1 = new GridBagConstraints();
		gbc_textField_1.gridwidth = 2;
		gbc_textField_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_1.insets = new Insets(0, 0, 5, 50);
		gbc_textField_1.gridx = 2;
		gbc_textField_1.gridy = 2;
		getContentPane().add(textField_1, gbc_textField_1);
		textField_1.setColumns(10);

		JLabel lblNewLabel = new JLabel("Titel");
		lblNewLabel.setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 50, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 3;
		getContentPane().add(lblNewLabel, gbc_lblNewLabel);

		JLabel label = new JLabel(":");
		label.setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.gridx = 1;
		gbc_label.gridy = 3;
		getContentPane().add(label, gbc_label);

		textField_2 = new JTextField();
		GridBagConstraints gbc_textField_2 = new GridBagConstraints();
		gbc_textField_2.gridwidth = 2;
		gbc_textField_2.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_2.insets = new Insets(0, 0, 5, 50);
		gbc_textField_2.gridx = 2;
		gbc_textField_2.gridy = 3;
		getContentPane().add(textField_2, gbc_textField_2);
		textField_2.setColumns(10);

		JLabel lblNewLabel_3 = new JLabel("Genre");
		lblNewLabel_3.setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagConstraints gbc_lblNewLabel_3 = new GridBagConstraints();
		gbc_lblNewLabel_3.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel_3.insets = new Insets(0, 50, 5, 5);
		gbc_lblNewLabel_3.gridx = 0;
		gbc_lblNewLabel_3.gridy = 4;
		getContentPane().add(lblNewLabel_3, gbc_lblNewLabel_3);

		JLabel label_1 = new JLabel(":");
		label_1.setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagConstraints gbc_label_1 = new GridBagConstraints();
		gbc_label_1.insets = new Insets(0, 0, 5, 5);
		gbc_label_1.anchor = GridBagConstraints.WEST;
		gbc_label_1.gridx = 1;
		gbc_label_1.gridy = 4;
		getContentPane().add(label_1, gbc_label_1);

		textField_3 = new JTextField();
		GridBagConstraints gbc_textField_3 = new GridBagConstraints();
		gbc_textField_3.gridwidth = 2;
		gbc_textField_3.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField_3.insets = new Insets(0, 0, 5, 50);
		gbc_textField_3.gridx = 2;
		gbc_textField_3.gridy = 4;
		getContentPane().add(textField_3, gbc_textField_3);
		textField_3.setColumns(10); // lol

		JButton btnAbbrechen = new JButton("Abbort");
		GridBagConstraints gbc_btnAbbrechen = new GridBagConstraints();
		gbc_btnAbbrechen.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnAbbrechen.insets = new Insets(0, 70, 0, 5);
		gbc_btnAbbrechen.gridx = 0;
		gbc_btnAbbrechen.gridy = 5;
		getContentPane().add(btnAbbrechen, gbc_btnAbbrechen);

		btnAbbrechen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// System.exit(0);
				MusicvotingGui_Voting.main(null);
				setVisible(false);
			}
		});

		JButton btnNewButton_1 = new JButton("Send");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MusicvotingGui_Voting.main(null);
				setVisible(false);
				logic.MusikwunschEintragen(new String(textField_1.getText()), new String(textField_2.getText()),
						new String(textField_3.getText()));
			}
		});
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.fill = GridBagConstraints.HORIZONTAL;
		gbc_btnNewButton_1.insets = new Insets(0, 30, 0, 30);
		gbc_btnNewButton_1.gridwidth = 2;
		gbc_btnNewButton_1.gridx = 1;
		gbc_btnNewButton_1.gridy = 5;
		getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}
}