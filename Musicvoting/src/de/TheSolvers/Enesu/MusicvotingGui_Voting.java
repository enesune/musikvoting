package de.TheSolvers.Enesu;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.lang.model.element.Element;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;

import de.TheSolvers.InguantaElias.LogicSchicht;
import de.TheSolvers.InguantaElias.Musikwunsch;

public class MusicvotingGui_Voting extends JFrame {

	private static final long serialVersionUID = 4070916292543141652L;
	private final static boolean true1 = false;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MusicvotingGui_Voting windows = new MusicvotingGui_Voting();
					windows.setResizable(true1); // actually true!
					windows.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MusicvotingGui_Voting() {

		// MusicvotingGui_Abschied abschiedGui= new MusicvotingGui_Abschied();
		LogicSchicht logic = new LogicSchicht();
		Musikwunsch wunsch = new Musikwunsch();

		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width / 2 - this.getSize().width / 2, dim.height / 2 - this.getSize().height / 2);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Sound of Axe and Fire");
		setSize(400, 300);
		setIconImage(Toolkit.getDefaultToolkit()
				.getImage(MusicvotingGui_Login.class.getResource("/de/TheSolvers/Enesu/SoundOfAxeAndFire.png")));
		getContentPane().setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] { 93, -96, 156, 0 };
		gridBagLayout.rowHeights = new int[] { 48, 170, 0, 0 };
		gridBagLayout.columnWeights = new double[] { 0.0, 1.0, 0.0, Double.MIN_VALUE };
		gridBagLayout.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		getContentPane().setLayout(gridBagLayout);

		JLabel lblHerzlichWillkommenBeim = new JLabel("Herzlich Willkommen beim Voting!");
		lblHerzlichWillkommenBeim.setFont(new Font("Tempus Sans ITC", Font.PLAIN, 20));
		GridBagConstraints gbc_lblHerzlichWillkommenBeim = new GridBagConstraints();
		gbc_lblHerzlichWillkommenBeim.gridwidth = 3;
		gbc_lblHerzlichWillkommenBeim.insets = new Insets(0, 0, 5, 0);
		gbc_lblHerzlichWillkommenBeim.gridx = 0;
		gbc_lblHerzlichWillkommenBeim.gridy = 0;
		getContentPane().add(lblHerzlichWillkommenBeim, gbc_lblHerzlichWillkommenBeim);

		JLabel lblListe = new JLabel("<html><body><center>Geordnete Musikliste f�r's Voting:</center></body></html>");
		lblListe.setFont(new Font("UD Digi Kyokasho NP-R", Font.PLAIN, 15));
		GridBagConstraints gbc_lblListe = new GridBagConstraints();
		gbc_lblListe.anchor = GridBagConstraints.NORTHEAST;
		gbc_lblListe.insets = new Insets(10, 25, 5, 5);
		gbc_lblListe.gridx = 0;
		gbc_lblListe.gridy = 1;
		getContentPane().add(lblListe, gbc_lblListe);

		String[] items = { "ab", "ac", "ba", "bc", "ca", "cb" };

		LogicSchicht musicSelection = new LogicSchicht();
		//JComboBox comboBox = new JComboBox (musicSelection.auswertung().toArray);
		JComboBox comboBox = new JComboBox (items);
		comboBox.setToolTipText("Musikliste");
		
		AutoCompletion.enable(comboBox);
		
		comboBox.setEditable(true);
		GridBagConstraints gbc_comboBox = new GridBagConstraints();
		gbc_comboBox.anchor = GridBagConstraints.NORTH;
		gbc_comboBox.gridwidth = 2;
		gbc_comboBox.insets = new Insets(10, 5, 5, 20);
		gbc_comboBox.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBox.gridx = 1;
		gbc_comboBox.gridy = 1;
		getContentPane().add(comboBox, gbc_comboBox);

		JButton btnNewButton = new JButton("abbrechen");
		GridBagConstraints gbc_btnNewButton = new GridBagConstraints();
		gbc_btnNewButton.insets = new Insets(0, 0, 0, 5);
		gbc_btnNewButton.gridx = 1;
		gbc_btnNewButton.gridy = 2;
		getContentPane().add(btnNewButton, gbc_btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MusicvotingGui_Abschied.main(null);
				setVisible(false);
			}

		});

		JButton btnNewButton_1 = new JButton("abschicken");
		GridBagConstraints gbc_btnNewButton_1 = new GridBagConstraints();
		gbc_btnNewButton_1.gridx = 2;
		gbc_btnNewButton_1.gridy = 2;
		getContentPane().add(btnNewButton_1, gbc_btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MusicvotingGui_Abschied.main(null);
				setVisible(false);
				wunsch.setTitel(new String(comboBox.getSelectedItem().toString()));
				logic.musikwunschWaehlen(wunsch);
			}

		});
	}

	// BENDENKE FALLS DER VOTE SCHON EXISTIERT;DANN HANDLE ALS OB ES IHN NICHT G�BE:
	// EINGEF�GT SOLL ER HEDOCH NICHT!

}
