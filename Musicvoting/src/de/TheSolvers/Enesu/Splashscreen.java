package de.TheSolvers.Enesu;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel; 	

public class Splashscreen extends JFrame {

	private static final long serialVersionUID = 813667855363635759L;

	GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
	int width = gd.getDisplayMode().getWidth();
	int height = gd.getDisplayMode().getHeight();
	static Timer timer = new Timer(true);
	
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					
					Splashscreen windows = new Splashscreen();
					windows.setResizable(false); // actually true!
					windows.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public Splashscreen() {
		setSize(640,452);
		setUndecorated(true);
		
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(Splashscreen.class.getResource("/de/TheSolvers/Enesu/TheSolvers_Splash.gif")));
		getContentPane().add(lblNewLabel, BorderLayout.CENTER);
		timer.schedule(new MyTask1(), 1000);
		try {
			setVisible(false);
			Thread.sleep(4000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	class MyTask1 extends TimerTask
	{
	   @Override
	   public void run()
	   {
		   
			//System.exit(0);
			MusicvotingGui_Login.main(null);
	   }
	}
}