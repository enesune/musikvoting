package de.TheSolvers.InguantaElias;

public class Musikwunsch {
String titel;
String bandname;
int id;
String genre;
int stimmenanzahl;

public Musikwunsch() {
	
}
public int getStimmenanzahl() {
	return this.stimmenanzahl;
}
public void setStimmenanzahl(int stimmenanzahl) {
	this.stimmenanzahl = stimmenanzahl;
}

public String getTitel() {
	return titel;
}
public void setTitel(String titel) {
	this.titel = titel;
}
public String getBandname() {
	return bandname;
}
public void setBandname(String bandname) {
	this.bandname = bandname;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getGenre() {
	return genre;
}
public void setGenre(String genre) {
	this.genre = genre;
}


}

