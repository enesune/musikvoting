package de.TheSolvers.MindPower;

import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.sun.xml.internal.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;

import de.TheSolvers.InguantaElias.Musikwunsch;
import de.TheSolvers.InguantaElias.Person;
import de.TheSolvers.InguantaElias.Vote;
import java.util.*;


public class Data 
{

	private int id = 0;
	private Person pers = new Person();
	private Vote v = new Vote();
	private Musikwunsch m = new Musikwunsch();
	
	private String driver = "com.mysql.jdbc.Driver"; //MySQL
	private String url = "jdbc:mysql://localhost/MusikVotingDatabase";
	private String user = "root";
	private String password = "";

	public void connect() {
		
		/**
		 * zun�chst findet eine Treiberregistrierung statt
		 * 
		 * dann wird ein neuer Nutzer im Datenbanksystem angelegt, welcher folgenderma�en aussieht:
		 * 
		 * CREATE USER 'mindpower'@'localhost' IDENTIFIED BY '*******';*/
		
		// Parameter f�r Verbindungsaufbau definieren
		

		try {
			// JDBC-Treiber laden
			
			Class.forName(driver);
			
			/*Connection wird hergestellt
			 * ________________________________________
			 */
			
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			
			// SQL-Anweisungen definieren
			
			/**
			Statement stmt = con.createStatement();
			stmt.execute("SELECT * FROM t_personen");
			stmt.execute("SELECT * FROM t_musikw�nsche");
			stmt.execute("SELECT * FROM t_votes");
			
			stmt.execute("INSERT INTO t_personen VALUES (" + this.id++ + "," + pers.getName() + " , " + pers.getAdresse() + ",");
			stmt.execute("INSERT INTO t_musikw�nsche (" + this.id++ + "," + m.getBandname() + "," + m.getGenre() + "," + m.getId() + "," + m.getTitel() + ",");
			stmt.execute("INSERT INTO t_votes (" + this.id++ + "," + v.getAenderungszahl() + ",");
			**/
			con.close();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

	}
	
	public Arraylist<String> getNamensListe()
	{
		try 
		{
			// JDBC-Treiber laden
			
			Class.forName(driver);
			
			/*Connection wird hergestellt
			 * ________________________________________
			 */
			
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(/*sql befehl */); 			//gibt alle namen zur�ck
			
			ArrayList<String> namensListe;		
			while (rs.next()) {
				namensListe.add(rs.getString(/*Spaltenname*/));
			}
			
			return namensListe;
		
		
		}catch (ClassNotFoundException | SQLException e) 
		  {
			e.printStackTrace();
	      }
		
		
	}
	
	public Arraylist<String> getTitelListe()
	{
		try 
		{
			// JDBC-Treiber laden
			
			Class.forName(driver);
			
			/*Connection wird hergestellt
			 * ________________________________________
			 */
			
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(/*sql befehl */); 			//gibt alle Titel zur�ck
			
			ArrayList<String> titelListe;		
			while (rs.next()) {
				titelListe.add(rs.getString(/*Spaltenname*/));
			}
			
			return titelListe;
		
		
		}catch (ClassNotFoundException | SQLException e) 
		  {
			e.printStackTrace();
	      }
		
		
	}
	
	public int getMusikwunschHoehsteId()
	{
		try 
		{
			// JDBC-Treiber laden
			
			Class.forName(driver);
			
			/*Connection wird hergestellt
			 * ________________________________________
			 */
			
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(/*sql befehl */); 			//gibt die h�hste Musikwunsch id zur�ck zur�ck
			
			int id;		
			while (rs.next()) {
				id = rs.getInt(/*Spaltenname*/); 		//id ist hier wahrscheinlich der name der spalte in der Datenbank (muss gegebenfalls ge�ndert werden)
			}
			
			return id;
		
		
		}catch (ClassNotFoundException | SQLException e) 
		  {
			e.printStackTrace();
	      }
		
		
	}
	
	public void setName(Person pers) 
	{
		try 
		{
			// JDBC-Treiber laden
			
			Class.forName(driver);
			
			/*Connection wird hergestellt
			 * ________________________________________
			 */
			
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement();
			
			stmt.execute(/*sql befehl */);		//tr�gt den namen pers.getName() ein
			
		} catch (ClassNotFoundException | SQLException e) 
		  {
			e.printStackTrace();
	      }
	}
	
	
	public void setMusikwunsch(Musikwunsch lied)
	{
		try 
		{
			// JDBC-Treiber laden
			
			Class.forName(driver);
			
			/*Connection wird hergestellt
			 * ________________________________________
			 */
			
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement(); 
			
			stmt.execute(/*sql befehl */); //tr�gt den Titel: lied.getTitel();, den bandnamen: lied.getBandname();, und das Gengre: lied.getGenre(); in die Datenbank
			
		} catch (ClassNotFoundException | SQLException e) 
		  {
			e.printStackTrace();
	      }
	}

	

	public int getVoteId()
	{
		try 
		{
			// JDBC-Treiber laden
		
			Class.forName(driver);
		
			/*Connection wird hergestellt
			 * ________________________________________
			 */
		
			Connection con;
			con = DriverManager.getConnection(url, user, password);
		
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(/*sql befehl */); 			//gibt die Id des h�hsten Votes von den Votes mit der niedrigsten �nderungszahl zur�ck
		
			int id;		
			while (rs.next()) {
				id = rs.getInt(/*Spaltenname*/);		
			}
		
			return id;
		
	
		}catch (ClassNotFoundException | SQLException e) 
			{
				e.printStackTrace();
			}
	
	
	}

	public void setVote(int id,Musikwunsch lied)
	{
		try 
		{
			// JDBC-Treiber laden
			
			Class.forName(driver);
			
			/*Connection wird hergestellt
			 * ________________________________________
			 */
			
			Connection con;
			con = DriverManager.getConnection(url, user, password);
			
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement(); 
			
			stmt.execute(/*sql befehl */); //den titel bandname und genre zur id l�schen und mit lied.getTitel();, lied.getBandname(); und lied.getGenre(); ersetzen
			
		} catch (ClassNotFoundException | SQLException e) 
		  {
			e.printStackTrace();
	      }
	}
	
	public Arraylist<String> getAuswertung()
	{
		try 
		{
			// JDBC-Treiber laden
		
			Class.forName(driver);
		
			/*Connection wird hergestellt
			 * ________________________________________
			 */
		
			Connection con;
			con = DriverManager.getConnection(url, user, password);
		
			// SQL-Anweisungen ausf�hren
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(/*sql befehl */); 		//gibt alle existenten Musikwunsche zu den votes aus der h�ufigkeit geordnet mit dem h�ufigsten angefangen 
		
			ArrayList<String> liste		
			while (rs.next()) {
			  liste.add(rs.getString("Musikwunsch"));		
			}
		
			return liste;
		
	
		}catch (ClassNotFoundException | SQLException e) 
			{
				e.printStackTrace();
			}
	
	
	}




}


